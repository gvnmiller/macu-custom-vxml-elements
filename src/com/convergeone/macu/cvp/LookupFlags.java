package com.convergeone.macu.cvp;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.convergeone.concurrent.TransactionState;
import com.convergeone.cvp.wrappers.CallDataWrapper;
import com.convergeone.cvp.wrappers.DecisionElementWrapper;
import com.convergeone.cvp.wrappers.ElementDataWrapper;
import com.convergeone.cvp.wrappers.ExitStateWrapper;
import com.convergeone.cvp.wrappers.SettingWrapper;
import com.convergeone.cvp.wrappers.settings.SingleSetting;
import com.convergeone.cvp.wrappers.settings.durations.RequiredSingleDurationSetting;
import com.convergeone.cvp.wrappers.settings.strings.RequiredSingleStringSetting;
import com.convergeone.logging.LogConfiguration;
import com.convergeone.type.Duration;

import com.convergeone.http.HTTPJSONReadOnlyTransaction;

public class LookupFlags extends DecisionElementWrapper {

	private static final Logger log = LogConfiguration.getLogger(LookupFlags.class);

	private static final SingleSetting<String> SETTING_URL = new RequiredSingleStringSetting("URL",
			"The URL to run the transaction against.");
	private static final SingleSetting<Duration> SETTING_TIMEOUT = new RequiredSingleDurationSetting("Timeout",
			"How long to wait before giving up on the transaction.");
	private static final SingleSetting<String> SETTING_FLAGID = new RequiredSingleStringSetting("FlagID",
			"The ID associated to the flag");

	private static final ElementDataWrapper ELEMENT_DATA_CLOSED = new ElementDataWrapper("Closed",
			"The value of the Closed flag.");
	private static final ElementDataWrapper ELEMENT_DATA_HOLIDAY = new ElementDataWrapper("Holiday",
			"The value of the Holiday flag.");
	private static final ElementDataWrapper ELEMENT_DATA_MEETING = new ElementDataWrapper("Meeting",
			"The value of the Meeting flag.");

	private static final ExitStateWrapper EXIT_STATE_DONE = new ExitStateWrapper("Done", "Found a matching flag.");
	private static final ExitStateWrapper EXIT_STATE_ERROR = new ExitStateWrapper("Error", "An error occurred.");

	@Override
	public String getElementName() {
		return "Get Flag Value";
	}

	@Override
	public String getDescription() {
		return "Get the value of a flag";
	}

	@Override
	public String getDisplayFolderName() {
		return "macu";
	}

	@Override
	public List<? extends SettingWrapper<?>> getSettingsList() {
		return Arrays.asList(SETTING_URL, SETTING_TIMEOUT, SETTING_FLAGID);
	}

	@Override
	public List<ElementDataWrapper> getElementDataList() {
		return Arrays.asList(ELEMENT_DATA_CLOSED, ELEMENT_DATA_HOLIDAY, ELEMENT_DATA_MEETING);
	}

	@Override
	public List<ExitStateWrapper> getExitStatesList() {
		return Arrays.asList(EXIT_STATE_DONE, EXIT_STATE_ERROR);
	}

	@Override
	public ExitStateWrapper execute(CallDataWrapper data) throws Throwable {
		log.entry(data);

		ExitStateWrapper exitState = EXIT_STATE_ERROR;
		String[] respList;

		String url = data.getSettingValue(SETTING_URL);
		Duration timeout = data.getSettingValue(SETTING_TIMEOUT);
		String flagID = data.getSettingValue(SETTING_FLAGID);

		HTTPJSONReadOnlyTransaction<String[]> transaction = new HTTPJSONReadOnlyTransaction<String[]>(
				url + "/" + flagID, String[].class, timeout);

		log.info("Starting transaction {}", transaction.transactionID);
		transaction.start();
		TransactionState state = transaction.waitAndTerminate();
		if (state.isComplete()) {
			log.info("Transaction {} complete.", transaction.transactionID);
			if (transaction.getResponseObject() != null) {
				respList = transaction.getResponseObject();

				data.setElementData(ELEMENT_DATA_CLOSED, "no");
				data.setElementData(ELEMENT_DATA_HOLIDAY, "no");
				data.setElementData(ELEMENT_DATA_MEETING, "no");

				for (String str : respList) {
					if (str.trim().contains("Closed")) {
						data.setElementData(ELEMENT_DATA_CLOSED, "yes");
					} else if (str.trim().contains("Holiday")) {
						data.setElementData(ELEMENT_DATA_HOLIDAY, "yes");
					} else if (str.trim().contains("Meeting")) {
						data.setElementData(ELEMENT_DATA_MEETING, "yes");
					}
				}
				
				exitState = EXIT_STATE_DONE;
				
			} else {
				exitState = EXIT_STATE_ERROR;
			}

		} else if (state.isError()) {
			log.error("The broadcast message lookup encountered an error.", transaction.getError());
			exitState = EXIT_STATE_ERROR;
		} else {
			log.fatal("The transaction is in an unexpected state {}.", state);
			exitState = EXIT_STATE_ERROR;
		}

		return log.exit(exitState);
	}

}