package com.convergeone.macu.cvp;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.convergeone.concurrent.TransactionState;
import com.convergeone.cvp.wrappers.CallDataWrapper;
import com.convergeone.cvp.wrappers.DecisionElementWrapper;
import com.convergeone.cvp.wrappers.ElementDataWrapper;
import com.convergeone.cvp.wrappers.ExitStateWrapper;
import com.convergeone.cvp.wrappers.SettingWrapper;
import com.convergeone.cvp.wrappers.settings.SingleSetting;
import com.convergeone.cvp.wrappers.settings.durations.RequiredSingleDurationSetting;
import com.convergeone.cvp.wrappers.settings.strings.RequiredSingleStringSetting;
import com.convergeone.logging.LogConfiguration;
import com.convergeone.macu.RoutingVariable;
import com.convergeone.type.Duration;

import com.convergeone.http.HTTPJSONReadOnlyTransaction;

public class LookupBMVariable extends DecisionElementWrapper {

	private static final Logger log = LogConfiguration.getLogger(LookupBMVariable.class);

	private static final SingleSetting<String> SETTING_URL = new RequiredSingleStringSetting("URL",
			"The URL to run the transaction against.");
	private static final SingleSetting<Duration> SETTING_TIMEOUT = new RequiredSingleDurationSetting("Timeout",
			"How long to wait before giving up on the transaction.");
	private static final SingleSetting<String> SETTING_VARIABLE = new RequiredSingleStringSetting("Variable",
			"The variable for the look up.");


	private static final ElementDataWrapper ELEMENT_DATA_VALUE = new ElementDataWrapper("Value",
			"The value of the variable.");
	
	
	private static final ExitStateWrapper EXIT_STATE_FOUND = new ExitStateWrapper("Found", "Found a matching.");
	private static final ExitStateWrapper EXIT_STATE_NOT_FOUND = new ExitStateWrapper("Not Found",
			"Did not find any matching.");
	private static final ExitStateWrapper EXIT_STATE_ERROR = new ExitStateWrapper("Error", "An error occurred.");

	@Override
	public String getElementName() {
		return "Get Broadcast Message Value";
	}

	@Override
	public String getDescription() {
		return "Get the value of a Broadcast Message";
	}

	@Override
	public String getDisplayFolderName() {
		return "macu";
	}

	@Override
	public List<? extends SettingWrapper<?>> getSettingsList() {
		return Arrays.asList(SETTING_URL, SETTING_TIMEOUT, SETTING_VARIABLE);
	}

	@Override
	public List<ElementDataWrapper> getElementDataList() {
		return Arrays.asList(ELEMENT_DATA_VALUE);
	}

	@Override
	public List<ExitStateWrapper> getExitStatesList() {
		return Arrays.asList(EXIT_STATE_FOUND, EXIT_STATE_NOT_FOUND, EXIT_STATE_ERROR);
	}

	@Override
	public ExitStateWrapper execute(CallDataWrapper data) throws Throwable {
		log.entry(data);

		ExitStateWrapper exitState = EXIT_STATE_ERROR;
		RoutingVariable resList;
				
		String value;

		String url = data.getSettingValue(SETTING_URL);
		Duration timeout = data.getSettingValue(SETTING_TIMEOUT);
		String variable = data.getSettingValue(SETTING_VARIABLE);
		
		
		
		HTTPJSONReadOnlyTransaction<RoutingVariable> transaction = new HTTPJSONReadOnlyTransaction<RoutingVariable>(url+"/"+variable, RoutingVariable.class, timeout);
		
		//HTTPTransaction transaction = new HTTPTransaction(url, "GET", timeout );

				
		log.info("Starting transaction {}", transaction.transactionID);
		transaction.start();
		TransactionState state = transaction.waitAndTerminate();
		if (state.isComplete()) {
			log.info("Transaction {} complete.", transaction.transactionID);
			if(transaction.getResponseObject() != null){
				resList = transaction.getResponseObject();
				value = resList.Value;
				
									
				data.setElementData(ELEMENT_DATA_VALUE, value);
							
				
				exitState = EXIT_STATE_FOUND;
			}else{
				exitState = EXIT_STATE_NOT_FOUND;
			}

		} else if (state.isError()) {
			log.error("The broadcast message lookup encountered an error.", transaction.getError());
			exitState = EXIT_STATE_ERROR;
		} else {
			log.fatal("The transaction is in an unexpected state {}.", state);
			exitState = EXIT_STATE_ERROR;
		}

		return exitState;
	}

	
	
}
